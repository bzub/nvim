" Debug {{{
set verbosefile=/tmp/nvim.log
set verbose=2
" }}}

" ----- Init ----- {{{
if &compatible
	set nocompatible
endif
" }}}

"----- Install Plugins ----- {{{
" dein {{{
" Add the dein installation directory into runtimepath
set runtimepath+=/home/bzub/.cache/vim/dein/repos/github.com/Shougo/dein.vim
if dein#load_state('/home/bzub/.cache/vim/dein')
	call dein#begin('/home/bzub/.cache/vim/dein')
	call dein#add('Shougo/dein.vim')
	" }}}

	" Cosmetic {{{
	call dein#add('icymind/NeoSolarized')
	call dein#add('vim-airline/vim-airline')
	call dein#add('vim-airline/vim-airline-themes')
	call dein#add('lilydjwg/colorizer')
	" call dein#add('terryma/vim-smooth-scroll')
	call dein#add('yuttie/comfortable-motion.vim')
	call dein#add('inside/vim-search-pulse')
	" }}}

	" Snippets {{{
	call dein#add('Shougo/neosnippet.vim')
	call dein#add('Shougo/neosnippet-snippets')
	" }}}

	" Completion {{{
	" let g:deoplete#enable_at_startup = 1
	" call dein#add('Shougo/deoplete.nvim', {'lazy': 1, 'rev': '*'})
	" " call dein#add('Shougo/deoplete.nvim', {'lazy': 1, 'rev': 'c7d31a36d4e39d16dd5a3141e512fefcaea05d79'})
	" if !has('nvim')
	" 	call dein#add('roxma/nvim-yarp')
	" 	call dein#add('roxma/vim-hug-neovim-rpc')
	" endif
	" call dein#add('hashivim/vim-terraform')
	" call dein#add('zchee/deoplete-jedi')
	" call dein#add('neomake/neomake')
	" call dein#add('juliosueiras/vim-terraform-completion')
	" }}}

	" c/cpp {{{
	call dein#add('tweekmonster/deoplete-clang2', {
					\'lazy': 1,
					\'on_ft': ['c', 'cc', 'cpp', 'objc']})
	" }}}

	" Go {{{
	call dein#add('zchee/deoplete-go', {
					\'build': 'make',
					\'on_ft': 'go',
					\'lazy': 1})
	call dein#add('fatih/vim-go', {
					\'on_ft': 'go'})
	" }}}

	" Git {{{
	call dein#add('lambdalisue/gina.vim')
	" }}}

	" Other {{{
	call dein#add('tomtom/tcomment_vim')
	call dein#add('uarun/vim-protobuf')
	" }}}

	" End
	call dein#end()
	call dein#save_state()
endif
call dein#remote_plugins() " }}}

"----- Plugin Hooks ----- {{{

" deoplete {{{
function! s:deoplete_pre()
	function! s:check_back_space() abort
		let col = col('.') - 1
		return !col || getline('.')[col - 1]  =~ '\s'
	endfunction

	inoremap <silent><expr> <TAB>
					\ pumvisible() ? "\<C-n>" :
					\ <SID>check_back_space() ? "\<TAB>" :
					\ deoplete#mappings#manual_complete()

	" let g:deoplete#omni_patterns = {}
	" call deoplete#custom#option({
	" 				\ 'omni_patterns': {
	" 					\ 'terraform': '[^ *\t"{=$]\w*',
	" 				\ }})
	" call deoplete#custom#source('omni', 'functions', {
	" 				\ 'terraform': 'terraformcomplete#Complete',
	" 				\ })
	" call deoplete#custom#var('omni', 'input_patterns', {})
endfunction

function! s:deoplete_setup()
	call deoplete#custom#option('num_processes', 2)
	" debug
	" call deoplete#custom#option('profile', v:true)
	" call deoplete#enable_logging('DEBUG', '/tmp/deoplete.log')
	" call deoplete#custom#source('asm', 'is_debug_enabled', 1)
	" call deoplete#custom#source('terraform', 'is_debug_enabled', 1)
	" call deoplete#custom#source('vim-terraform', 'is_debug_enabled', 1)
	" call deoplete#custom#source('vim-terraform-completion', 'is_debug_enabled', 1)
endfunction

if dein#tap('deoplete.nvim')
	call dein#set_hook('deoplete.nvim', 'hook_add',
					\function('s:deoplete_pre'))
	call dein#set_hook('deoplete.nvim', 'hook_post_source',
					\function('s:deoplete_setup'))
endif
" }}}

" neosnippet {{{
function! s:neosnippet_setup()
	imap <C-k>     <Plug>(neosnippet_expand_or_jump)
	smap <C-k>     <Plug>(neosnippet_expand_or_jump)
	xmap <C-k>     <Plug>(neosnippet_expand_target)

	" SuperTab like snippets behavior.
	" Note: It must be "imap" and "smap".  It uses <Plug> mappings.
	imap <expr><TAB>
					\ pumvisible() ? "\<C-n>" :
					\ neosnippet#expandable_or_jumpable() ?
					\    "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"
	smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
					\ "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"

	" For conceal markers.
	if has('conceal')
		set conceallevel=2 concealcursor=niv
	endif
endfunction

if dein#tap('neosnippet.vim')
	call dein#set_hook('neosnippet.vim', 'hook_add',
					\function('s:neosnippet_setup'))
endif
" }}}

" tweekmonster/deoplete-clang2 {{{
function! s:deoplete_clang2_setup()
	let g:deoplete#sources#clang#flags = [
					\'-I/opt/Espressif/esp-open-sdk/xtensa-lx106-elf/xtensa-lx106-elf/sysroot/usr/include',
					\'-I/mongoose-os',
					\'-I/mongoose-os/fw/include',
					\'-I/mongoose-os/fw/src',
					\'-I/mongoose-os/mongoose',
					\'-I/mongoose-os/frozen',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/build/gen',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/nanopb/include',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/atca/cryptoauthlib/lib',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/atca/cryptoauthlib/lib/basic/',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/atca/cryptoauthlib/lib/host/',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/atca/include',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/atca/src/',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/core/include',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/core/include/esp8266',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/core/src/',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/core/src/esp8266/',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/gcp/src/',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/i2c/include',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/i2c/src/',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/i2c/src/common_gpio/',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/mbedtls/include',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/mbedtls/include/esp8266',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/mbedtls/mbedtls/include',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/mongoose/include',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/mqtt/include',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/mqtt/src/',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/rpc-common/include',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/rpc-common/include/mg_rpc',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/rpc-common/src/',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/rpc-common/src/mg_rpc/',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/rpc-service-config/include',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/rpc-service-config/src/',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/rpc-service-fs/include',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/rpc-service-fs/src/',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/rpc-uart/include',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/rpc-uart/src/',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/sntp/src/',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/vfs-common/include',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/vfs-common/include/esp8266',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/vfs-common/src/',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/vfs-common/src/esp8266/',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/vfs-dev-part/include',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/vfs-dev-part/src/',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/vfs-fs-spiffs/include',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/vfs-fs-spiffs/include/esp8266',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/vfs-fs-spiffs/include/spiffs',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/vfs-fs-spiffs/src/',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/vfs-fs-spiffs/src/spiffs/',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/wifi/esp8266/src/',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/wifi/include',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/deps/wifi/src/',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/include',
					\'-I/home/bzub/go/src/gitlab.com/agamigo/micro-amigo/src/',
					\'-I/mongoose-os/common/spiffs',
					\'-I/mongoose-os/fw/platforms/esp8266/include',
					\'-I/mongoose-os/fw/platforms/esp8266/include/spiffs',
					\'-I/mongoose-os/common/platforms/esp8266/rboot/rboot',
					\'-I/opt/Espressif/ESP8266_NONOS_SDK/include/lwip',
					\'-I/opt/Espressif/ESP8266_NONOS_SDK',
					\'-I/opt/Espressif/ESP8266_NONOS_SDK/include',
					\'-I/mongoose-os/common/platforms/esp8266',
					\'-I/mongoose-os/common/platforms/esp/src',
					\'-I/mongoose-os/fw/platforms/esp8266/src',
					\'-I/mongoose-os/common/mg_rpc',
					\'-I/opt/Espressif/cs_lwip/src/include',
					\'-I/opt/Espressif/cs_lwip/src/include/ipv4',
					\'-I/opt/Espressif/cs_lwip/espressif/include',
					\'-I/home/bzub/projects/micro-amigo/build/gen',
					\]
endfunction

if dein#tap('deoplete-clang2')
	call dein#set_hook('deoplete-clang2', 'hook_add',
					\function('s:deoplete_clang2_setup'))
endif
" }}}

" vim-go {{{
function! s:vim_go_setup()
	let g:go_def_mapping_enabled = 0
	let g:go_loaded_gosnippets = 1
	let g:go_snippet_engine = 'neosnippet'
	let g:go_highlight_extra_types = 1
	let g:go_highlight_operators = 1
	let g:go_auto_type_info = 0
	let g:go_bin_path = '/home/bzub/go/bin/vim-go'
endfunction
if dein#tap('vim-go')
	call dein#set_hook('vim-go', 'hook_add',
					\function('s:vim_go_setup'))
endif
" }}}

" vim-airline {{{
function! s:vim_airline_setup()
	let g:airline#extensions#tabline#enabled = 1
endfunction
if dein#tap('vim-airline')
	call dein#set_hook('vim-airline', 'hook_add',
					\function('s:vim_airline_setup'))
endif
" }}}

" deoplete-go {{{
function! s:deoplete_go_setup()
	" let g:deoplete#sources#go#gocode_binary = $GOPATH.'/bin/gocode'
	let g:deoplete#sources#go#sort_class = ['package', 'func', 'type', 'var', 'const']
	let g:deoplete#sources#go#builtin_objects = 0
	let g:deoplete#sources#go#source_importer = 0
endfunction

if dein#tap('deoplete-go')
	call dein#set_hook('deoplete-go', 'hook_add',
					\function('s:deoplete_go_setup'))
endif
" }}}

" vim-terraform {{{
function! s:vim_terraform_setup()
	let g:terraform_fmt_on_save=1
	let g:terraform_align=0
	let g:terraform_fold_sections=1
	let g:terraform_remap_spacebar=1
endfunction

if dein#tap('vim-terraform')
	call dein#set_hook('vim-terraform', 'hook_add',
					\function('s:vim_terraform_setup'))
endif
" }}}

" vim-terraform-completion {{{
function! s:vim_terraform_completion_setup()
	let g:terraform_versions_config=g:dein#_base_path . '/repos/github.com/juliosueiras/vim-terraform-completion/.tfcompleterc'
	" (Optional) Default: 0, enable(1)/disable(0) plugin's keymapping
	" let g:terraform_completion_keys = 0

	" (Optional) Default: 1, enable(1)/disable(0) terraform module registry completion
	let g:terraform_registry_module_completion = 1
endfunction

if dein#tap('vim-terraform-completion')
	call dein#set_hook('vim-terraform-completion', 'hook_add',
					\function('s:vim_terraform_completion_setup'))
endif
" }}}

" vim-smooth-scroll {{{
function! s:vim_smooth_scroll_setup()
	noremap <silent> <c-u> :call smooth_scroll#up(&scroll, 5, 2)<CR>
	noremap <silent> <c-d> :call smooth_scroll#down(&scroll, 5, 2)<CR>
	noremap <silent> <c-b> :call smooth_scroll#up(&scroll*2, 5, 4)<CR>
	noremap <silent> <c-f> :call smooth_scroll#down(&scroll*2, 5, 4)<CR>
endfunction

if dein#tap('vim-smooth-scroll')
	call dein#set_hook('vim-smooth-scroll', 'hook_add',
		\function('s:vim_smooth_scroll_setup'))
endif
" }}}

" comfortable-motion.vim {{{
" function! s:comfortable_motion_setup()
" endfunction
"
" if dein#tap('comfortable-motion.vim')
" 	call dein#set_hook('comfortable-motion.vim', 'hook_add',
" 		\function('comfortable_motion_setup'))
" endif
" }}}

" vim-search-pulse {{{
function! s:vim_search_pulse_setup()
	let g:vim_search_pulse_duration = 200
endfunction

if dein#tap('vim-search-pulse')
	call dein#set_hook('vim-search-pulse', 'hook_add',
		\function('s:vim_search_pulse_setup'))
endif
" }}}
" }}}

"----- Editor customizations ----- {{{
behave xterm
set colorcolumn=80
set completeopt+=noinsert
set completeopt+=noselect
set nowrap
" Colorscheme
set termguicolors
set bg=dark
colorscheme NeoSolarized
" Hide Info(Preview) window after completions
" autocmd CursorMovedI * if pumvisible() == 0|pclose|endif
autocmd InsertLeave * if pumvisible() == 0|pclose|endif

filetype plugin indent on
syntax enable
" }}}

"----- User Functions ----- {{{
function! RemoveUnusedPlugins()
	call map(dein#check_clean(), "delete(v:val, 'rf')")
	call dein#recache_runtimepath()
endfunction
" }}}

" VimEnter autocmd {{{
function! s:vim_enter()
	" call dein#remote_plugins()
	" Ensure post_sourse hooks run
	" call deoplete#initialize()
	call dein#call_hook('post_source')
	" call deoplete#enable()
endfunction

autocmd VimEnter * call s:vim_enter()
" }}}

if exists('g:loaded_hello')
	finish
endif
let g:loaded_hello = 1

function! s:Requirehello(host) abort
	" 'hello' is the binary created by compiling the program above.
	return jobstart(['hello'], {'rpc': v:true})
endfunction

call remote#host#Register('hello', 'x', function('s:Requirehello'))
" The following lines are generated by running the program
" command line flag --manifest hello
call remote#host#RegisterPlugin('hello', '0', [
				\ {'type': 'function', 'name': 'Hello', 'sync': 1, 'opts': {}},
				\ ])

" vim: set foldmethod=marker ts=2 sw=2 tw=80 noet :
